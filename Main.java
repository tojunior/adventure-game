import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {
    static String name;
    static String randomBenefit;
    static int x = 1;
    static int y = 1;
    static ArrayList<String> items = new ArrayList<>();
    static ArrayList<String> benefits = new ArrayList<String>();


   static Student Zosia = new Student(true,false,true,true, true);
   static Student Wojtek = new Student(true,false,true,true, true);
   static Student Karolina = new Student(false,false,false,false, false);
   static Student Bartek = new Student(true,false,true,false, false);
   static Student Darek = new Student(true,true,true,true, true);

    static Scanner scan = new Scanner(System.in);




   static void gotPen(){
        if(items.contains("Dlugopis")){
            System.out.println("Zdobyles dlugopis! Zdobadz zadanie domowe od innych uczniow i uzyj dlugopisu do przepisania.");
            System.out.println("");
        }
    }
    static void takePen(){
        if(Zosia.BorrowPen.equals(true)){
            System.out.println("Kliknij 1 aby zabrac dlugopis.");
            int take = scan.nextInt();
                if(take == 1){
                    items.add("Dlugopis");
                    gotPen();
                }
        }
        if(Wojtek.BorrowPen.equals(true)){
            System.out.println("Kliknij 1 aby zabrac dlugopis.");
            int take = scan.nextInt();
                if(take == 1){
                    items.add("Dlugopis");
                    gotPen();
                }
        }
        if(Karolina.BorrowPen.equals(true)){
            System.out.println("Kliknij 1 aby zabrac dlugopis.");
            int take = scan.nextInt();
                if(take == 1){
                    items.add("Dlugopis");
                    gotPen();
                }
        }
        if(Bartek.BorrowPen.equals(true)){
            System.out.println("Kliknij 1 aby zabrac dlugopis.");
            int take = scan.nextInt();
                if(take == 1){
                    items.add("Dlugopis");
                    gotPen();
                }
        }
        if(Darek.BorrowPen.equals(true)){
            System.out.println("Kliknij 1 aby zabrac dlugopis.");
            int take = scan.nextInt();
                if(take == 1){
                    items.add("Dlugopis");
                    gotPen();
                }
        }
    }
    static void DrawBenefit(){
        Random r = new Random();
        int random = r.nextInt(benefits.size());
        randomBenefit = benefits.get(random);
        benefits.remove(random);

    }


    public static void main(String[] args) {
       benefits.add("Podziekowanie");
       benefits.add("Zrobienie zadania domowego z matematyki");
       benefits.add("Zaplacenie 10 zlotych");


       System.out.println("Podaj swoje imie: ");
        name = scan.nextLine();
        System.out.println("");
        System.out.println("Witaj w grze przygodowej " + name + "!\nGra toczy sie w Twojej szkole na lekcji jezyka polskiego.\n" +
                "Nauczycielka niespodziewanie mowi, ze kazda osoba wedlug dziennika podchodzi do niej i przedstawia swoje zadanie domowe.\n" +
                "Zadaniem bylo opisac na 50 slow biografie Adama Mickiewicza\n"+
                "Jak zwykle zapomniales zrobic zadania domowego, ale Twoj numer w dzienniku to 6, wiec masz troche czasu, aby spisac zadanie.\n" +
                "Brak zadania domowego grozi ocena niedostateczna, a tym samym nie zdaniem klasy.\n" +
                "Zalezy Ci aby zdac klase, bo inaczej nie pojedziesz na wymarzone wakacje.\n" +
                "Zrob wszystko, aby spisac zadanie od kolegow i kolezanek z klasy, lecz nie kazdy z nich bedzie chetny by Ci je dac lub bedzie chcial cos w zamian.\n" +
                "Nauczycielka ma krotkowzrocznosc i zapomniala tego dnia okularow co ulatwi ci przepisanie zadania :)\n" +"Sprawdz liste zadan.\n"+
                "Powodzenia !");
        System.out.println("");


        while(true) {
            System.out.println("1.Lista zadan\n2.Opisz pomieszczenie\n3.Przedmioty\n4.Pomoc\n5.Popros o dlugopis\n6.Popros o zadanie\n");
            int ChooseAction = scan.nextInt();
            switch (ChooseAction) {
                case 1:
                    System.out.println("* Zapytaj o dlugopis i go zabierz.\n* Zdobadz zadanie domowe od innych uczniow, lecz wybierz osobe ktora bedzie chciala jak najmniej w zamian .\n* Uzyj dlugopisu do przepisania zadania domowego.");
                    System.out.println("");
                    continue;
                case 2:
                    System.out.println("Klasa sklada sie z 3 rzedow po 2 lawki i biurka nauczycielki na przodzie klasy. Siedzisz w drugiej lawce w srodkowym rzedzie" +
                            ",a przed Toba kolega, ktory Cie zaslania przez co bedziesz mogl latwiej przepisac zadanie.");
                    System.out.println("");
                    break;
                case 3:
                    System.out.println("Twoja lista przedmiotow:");
                    System.out.println(items);
                    System.out.println("");
                    break;
                case 4:
                    System.out.println("I.Lista zadan - opisuje liste zadan do zrobienia.\nII.Opisz - opisuje pomieszczenie, w ktorym sie znajdujesz.\n" +
                            "III.Przedmioty - opisuje liste Twoich przedmiotow.\n");
                    continue;
                case 5:
                while (!items.contains("Dlugopis")){
                    System.out.println("Kogo chcesz poprosic o dlugopis?\n" +
                            "1.Zosia\n2.Wojtek\n3.Karolina\n4.Bartek\n5.Darek");
                    int pencil = scan.nextInt();
                    switch (pencil) {
                        case 1 -> {
                            System.out.println("Czy Zosia posiada dlugopis?");
                            if (Zosia.Pencil.equals(true)) {
                                System.out.println("Posiada dlugopis");
                                System.out.println("Czy pozyczy dlugopis?");
                                if (Zosia.BorrowPen.equals(true)) {
                                    System.out.println("Zosia pozyczy dlugopis");
                                    takePen();
                                    continue;
                                } else System.out.println("Zosia nie pozyczy dlugopisu.\n" +
                                        "Popros innych kolegow/kolezanki o dlugopis");
                                System.out.println("");

                            } else
                                System.out.println("Nie posiada dlugopisu\nPopros innych kolegow/kolezanki o dlugopis.");
                        }
                        case 2 -> {
                            System.out.println("Czy Wojtek posiada dlugopis?");
                            if (Wojtek.Pencil.equals(true)) {
                                System.out.println("Posiada dlugopis");
                                System.out.println("Czy pozyczy dlugopis?");
                                if (Wojtek.BorrowPen.equals(true)) {
                                    System.out.println("Wojtek pozyczy dlugopis");
                                    System.out.println("");
                                    takePen();
                                    continue;
                                } else System.out.println("Wojtek nie pozyczy dlugopisu.\n" +
                                        "Popros innych kolegow/kolezanki o dlugopis");
                                System.out.println("");

                            } else
                                System.out.println("Nie posiada dlugopisu\nPopros innych kolegow/kolezanki o dlugopis.");
                            System.out.println("");

                        }
                        case 3 -> {
                            System.out.println("Czy Karolina posiada dlugopis?");
                            if (Karolina.Pencil.equals(true)) {
                                System.out.println("Posiada dlugopis");
                                System.out.println("Czy pozyczy dlugopis?");
                                if (Karolina.BorrowPen.equals(true)) {
                                    System.out.println("Karolina pozyczy dlugopis");
                                    System.out.println("");
                                    takePen();
                                    continue;
                                } else System.out.println("Karolina nie pozyczy dlugopisu.\n" +
                                        "Popros innych kolegow/kolezanki o dlugopis");
                                System.out.println("");

                            } else
                                System.out.println("Nie posiada dlugopisu\nPopros innych kolegow/kolezanki o dlugopis.");
                            System.out.println("");

                        }
                        case 4 -> {
                            System.out.println("Czy Bartek posiada dlugopis?");
                            if (Bartek.Pencil.equals(true)) {
                                System.out.println("Posiada dlugopis");
                                System.out.println("Czy pozyczy dlugopis?");
                                if (Bartek.BorrowPen.equals(true)) {
                                    System.out.println("Bartek pozyczy dlugopis");
                                    System.out.println("");
                                    takePen();
                                    continue;
                                } else System.out.println("Bartek nie pozyczy dlugopisu.\n" +
                                        "Popros innych kolegow/kolezanki o dlugopis");
                                System.out.println("");
                                ;
                            } else
                                System.out.println("Nie posiada dlugopisu\nPopros innych kolegow/kolezanki o dlugopis.");
                            System.out.println("");

                        }
                        case 5 -> {
                            System.out.println("Czy Darek posiada dlugopis?");
                            if (Darek.Pencil.equals(true)) {
                                System.out.println("Posiada dlugopis");
                                System.out.println("Czy pozyczy dlugopis?");
                                if (Darek.BorrowPen.equals(true)) {
                                    System.out.println("Darek pozyczy dlugopis");
                                    System.out.println("");
                                    takePen();
                                    continue;
                                } else System.out.println("Darek nie pozyczy dlugopisu.\n" +
                                        "Popros innych kolegow/kolezanki o dlugopis");
                                System.out.println("");
                            } else
                                System.out.println("Nie posiada dlugopisu\nPopros innych kolegow/kolezanki o dlugopis.");
                            System.out.println("");
                        }
                    }
            }
                case 6:
                    if (!items.contains("Dlugopis")) {
                        System.out.println("Nie masz dlugopisu! Zdobadz go zanim zapytasz o zadanie domowe.");
                        System.out.println("");
                        continue;
                    }
                    System.out.println("Zapytaj kazdego po kolei o zadanie domowe, aby sprawdzic kto chce jak najmniej w zamian");
                    while(x<6) {
                        System.out.println("Kogo chcesz zapytac o zadanie domowe ?\n" +
                                "1.Zosia\n2.Wojtek\n3.Karolina\n4.Bartek\n5.Darek");
                        int homework = scan.nextInt();
                        switch (homework) {
                            case 1 -> {
                                x = x + 1;
                                System.out.println("Czy Zosia posiada zadanie domowe?");
                                if (Zosia.HaveHomework.equals(true)) {
                                    System.out.println("Posiada zadanie domowe");
                                    System.out.println("Czy da przepisac zadanie domowe?");
                                    if (Zosia.ShareHomework.equals(true)) {
                                        System.out.println("Zosia da przepisac zadanie domowe");
                                        if (Zosia.InReturn.equals(true)) {
                                            System.out.println("");
                                            System.out.println("Co chce w zamian?");
                                            DrawBenefit();
                                            System.out.println(randomBenefit);
                                            benefits.remove(randomBenefit);
                                            System.out.println("");
                                            continue;
                                        }
                                    } else System.out.println("Zosia nie da przepisac zadania domowego.\n" +
                                            "Popros innych kolegow/kolezanki o zadanie domowe");
                                    System.out.println("");
                                    continue;

                                } else
                                    System.out.println("Nie posiada zadania domowego\nPopros innych kolegow/kolezanki o dlugopis.");
                            }
                            case 2 -> {
                                x = x + 1;
                                System.out.println("Czy Wojtek posiada zadanie domowe?");
                                if (Wojtek.HaveHomework.equals(true)) {
                                    System.out.println("Posiada zadanie domowe");
                                    System.out.println("Czy da przepisac zadanie domowe?");
                                    if (Wojtek.ShareHomework.equals(true)) {
                                        System.out.println("Wojtek da przepisac zadanie domowe");
                                        if (Wojtek.InReturn.equals(true)) {
                                            System.out.println("");
                                            System.out.println("Co chce w zamian?");
                                            DrawBenefit();
                                            System.out.println(randomBenefit);
                                            benefits.remove(randomBenefit);
                                            System.out.println("");
                                        }
                                        continue;
                                    } else System.out.println("Wojtek nie da przepisac zadania domowego.\n" +
                                            "Popros innych kolegow/kolezanki o zadanie domowe");
                                    System.out.println("");
                                    continue;

                                } else
                                    System.out.println("Nie posiada zadania domowego\nPopros innych kolegow/kolezanki o dlugopis.");
                            }
                            case 3 -> {
                                x = x + 1;
                                System.out.println("Czy Karolina posiada zadanie domowe?");
                                if (Karolina.HaveHomework.equals(true)) {
                                    System.out.println("Posiada zadanie domowe");
                                    System.out.println("Czy da przepisac zadanie domowe?");
                                    if (Karolina.ShareHomework.equals(true)) {
                                        System.out.println("Karolina da przepisac zadanie domowe");
                                        if (Karolina.InReturn.equals(true)) {
                                            System.out.println("");
                                            System.out.println("Co chce w zamian?");
                                            DrawBenefit();
                                            System.out.println(randomBenefit);
                                            benefits.remove(randomBenefit);
                                            System.out.println("");
                                        }
                                        continue;
                                    } else System.out.println("Karolina nie da przepisac zadania domowego.\n" +
                                            "Popros innych kolegow/kolezanki o zadanie domowe\n" +
                                            "");

                                    System.out.println("");
                                    continue;

                                } else
                                    System.out.println("Nie posiada zadania domowego\nPopros innych kolegow/kolezanki o dlugopis.\n" +
                                            "");
                            }
                            case 4 -> {
                                x = x + 1;
                                System.out.println("Czy Bartek posiada zadanie domowe?");
                                if (Bartek.HaveHomework.equals(true)) {
                                    System.out.println("Posiada zadanie domowe");
                                    System.out.println("Czy da przepisac zadanie domowe?");
                                    if (Bartek.ShareHomework.equals(true)) {
                                        System.out.println("Bartek da przepisac zadanie domowe");
                                        if (Bartek.InReturn.equals(true)) {
                                            System.out.println("");
                                            System.out.println("Co chce w zamian?");
                                            DrawBenefit();
                                            System.out.println(randomBenefit);
                                            benefits.remove(randomBenefit);
                                            System.out.println("");
                                        }
                                        continue;
                                    } else System.out.println("Bartek nie da przepisac zadania domowego.\n" +
                                            "Popros innych kolegow/kolezanki o zadanie domowe\n" +
                                            "");
                                    System.out.println("");
                                    continue;

                                } else
                                    System.out.println("Nie posiada zadania domowego\nPopros innych kolegow/kolezanki o dlugopis.\n" +
                                            "");

                            }
                            case 5 -> {
                                x = x + 1;
                                System.out.println("Czy Darek posiada zadanie domowe?");
                                if (Darek.HaveHomework.equals(true)) {
                                    System.out.println("Posiada zadanie domowe");
                                    System.out.println("Czy da przepisac zadanie domowe?");
                                    if (Darek.ShareHomework.equals(true)) {
                                        System.out.println("Darek da przepisac zadanie domowe");
                                        if (Darek.InReturn.equals(true)) {
                                            System.out.println("");
                                            System.out.println("Co chce w zamian?");
                                            DrawBenefit();
                                            System.out.println(randomBenefit);
                                            benefits.remove(randomBenefit);
                                            System.out.println("");
                                        }
                                        continue;
                                    } else System.out.println("Darek nie da przepisac zadania domowego.\n" +
                                            "Popros innych kolegow/kolezanki o zadanie domowe");
                                    System.out.println("");
                                    continue;

                                } else
                                    System.out.println("Nie posiada zadania domowego\nPopros innych kolegow/kolezanki o dlugopis.\n" +
                                            "");
                                continue;
                            }
                        }
                    }
                        System.out.println("Od kogo chcesz wziac zadanie do przepisania?");
                        if(Zosia.ShareHomework){
                            System.out.println("Zosia");
                        }if(Wojtek.ShareHomework){
                            System.out.println("Wojtek");
                        }if(Karolina.ShareHomework){
                            System.out.println("Karolina");
                        }if(Bartek.ShareHomework){
                            System.out.println("Bartek");
                        }if(Darek.ShareHomework){
                            System.out.println("Darek");
                        }
                        int chooseStudent = scan.nextInt();
                        switch (chooseStudent) {
                            case 1:
                                items.add("Zadanie domowe do przepisania");
                            case 2:
                                items.add("Zadanie domowe do przepisania");
                            case 3:
                                items.add("Zadanie domowe do przepisania");
                        }

                        if(items.contains("Zadanie domowe do przepisania")){
                            System.out.println("Gratulacje! Zdobyles zadanie domowe do przepisania.");
                            System.out.println("");
                            System.out.println("Kliknij 1 aby uzyc dlugopisu i przepisac zadanie domowe");
                            int usePen = scan.nextInt();
                            if(usePen == 1)
                                System.out.println("Przepisujesz zadanie ...");
                                System.out.println("...");
                                System.out.println("...");
                                System.out.println("Nauczycielka wstaje z krzesla rozgladnac sie po klasie ... co robisz?");
                                System.out.println("");
                                System.out.println("1.Przestajsz pisac aby nie zwrocic na siebie uwagi.\n2.Piszesz dalej, poniewaz boisz sie, ze nie zdazysz, lecz jest to ryzykowne.");
                                int chooseDecision =scan.nextInt();
                                if(chooseDecision == 1){
                                    System.out.println("Nauczycielka usiadla, a ty kontynuujesz przepisywanie zadania...");
                                    System.out.println("...");
                                }
                                if(chooseDecision == 2) {
                                    System.out.println("Nauczycielka zauwaza, ze piszesz bardzo nerowo i szybko cos w zeszycie...\n" +
                                            "Postanawia sprawdzic co robisz i nakrywa Cie na przepisywaniu zadania. Bardzo sie oburzyla i mimo Twoich prosb wstawila Ci ocene niedostateczna i tym samym nie zdasz do nastepnej klasy..." +
                                            "Szkoda.");
                                    System.out.println("Sprobuj jeszcze raz.");
                                    System.exit(0);
                                }
                                System.out.println("...");
                                System.out.println("Udalo Ci sie przepisac zadanie! Zanosisz nauczycielce zadanie do sprawdzenia. ");
                                items.add("Zadanie domowe");
                                items.remove("Zadanie domowe do przepisania");
                                System.out.println("");
                        }
                        else System.out.println("Nie posiadasz zadania do przepisania. Zdobadz je !");
                        System.out.println("");


            }
            if(items.contains("Zadanie domowe")){
                System.out.println("Gratulacje! Udalo Ci sie przepisac zadanie domowe i uzyskac ocene pozytywna ! Nie jestes zagrozony nie zdaniem klasy.");
                System.exit(0);
            }
        }
    }
}
